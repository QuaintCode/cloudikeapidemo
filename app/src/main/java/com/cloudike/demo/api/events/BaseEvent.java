package com.cloudike.demo.api.events;

import com.cloudike.demo.api.RequestTicket;

public abstract class BaseEvent<T> {

    private RequestTicket mTicket;

    BaseEvent(RequestTicket ticket) {
        mTicket = ticket;
    }

    public RequestTicket getTicket() {
        return mTicket;
    }

    public abstract T getData();

}
