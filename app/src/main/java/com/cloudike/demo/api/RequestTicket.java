package com.cloudike.demo.api;

public class RequestTicket {

    private static long sNextId = 0;

    private long mId;

    private RequestTicket(long id) {
        mId = id;
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public static RequestTicket newInstance() {
        return new RequestTicket(++sNextId);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }
        RequestTicket other = (RequestTicket) obj;

        if (mId != other.mId) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return Long.valueOf(mId).hashCode();
    }

}
