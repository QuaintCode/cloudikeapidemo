package com.cloudike.demo.api.events;

import com.cloudike.demo.api.RequestTicket;

public class FileMetadataEvent<FileMetadata> extends BaseEvent {

    private FileMetadata mFileMeta;

    public FileMetadataEvent(RequestTicket ticket, FileMetadata data) {
        super(ticket);
        mFileMeta = data;
    }

    @Override
    public FileMetadata getData() {
        return mFileMeta;
    }
}
