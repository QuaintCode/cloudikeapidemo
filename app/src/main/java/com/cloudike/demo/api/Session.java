package com.cloudike.demo.api;

public class Session {
    private static String sToken;
    private static long sExpired;

    public static String getToken() {
        return sToken;
    }

    public static void setToken(String token, long expired) {
        sToken = token;
        sExpired = expired;
    }

    public static boolean isExpired() {
        return sToken != null && System.currentTimeMillis() > sExpired;
    }
}
