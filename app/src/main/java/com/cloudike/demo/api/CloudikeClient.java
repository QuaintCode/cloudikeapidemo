package com.cloudike.demo.api;

import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.cloudike.demo.api.builder.FileMetadataParamsBuilder;
import com.cloudike.demo.api.builder.ParamsBuilder;
import com.cloudike.demo.api.events.BaseEvent;
import com.cloudike.demo.api.events.ErrorEvent;
import com.cloudike.demo.api.events.FileMetadataEvent;
import com.cloudike.demo.api.events.LoginEvent;
import com.cloudike.demo.api.exception.SessionExpiredException;
import com.cloudike.demo.api.model.account.LoginData;
import com.cloudike.demo.api.model.files.FileMetadata;
import com.squareup.otto.ThreadEnforcer;

import java.util.HashMap;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.RetrofitError;

public class CloudikeClient {

    public enum Request {
        LOGIN,
        GET_METADATA
    }

    public class Job {
        private Request mRequest;
        private RequestTicket mTicket;
        private boolean mIsCanceled = false;
        private Bundle mOptions;

        public Job(Request req, Bundle opt) {
            mTicket = RequestTicket.newInstance();
            mRequest = req;
            mOptions = opt;
        }

        public Request getRequest() {
            return mRequest;
        }

        public RequestTicket getTicket() {
            return mTicket;
        }

        public synchronized void cancel() {
            mIsCanceled = true;
        }

        public synchronized boolean isCanceled() {
            return mIsCanceled;
        }

        public Bundle getOptions() {
            return mOptions;
        }
    }

    public class WorkerHandler extends Handler {

        private static final int MSG_DO_JOB = 0;
        private Job mCurrentJob;

        public WorkerHandler(Looper lopper) {
            super(lopper);
        }

        @Override
        public void handleMessage(Message msg) {
            Job job = (Job) msg.obj;
            // this job has been canceled before execution
            if (job.isCanceled()) {
                return;
            }
            mCurrentJob = job;
            // perform request
            BaseEvent data = execute(job);
            synchronized (this) {
                mEventBus.post(data);
                mCurrentJob = null;
            }
        }

        public void runJob(Job job) {
            sendMessage(obtainMessage(MSG_DO_JOB, job));
        }

        private BaseEvent execute(Job job) {
            try {
                return executeJob(job);
            } catch (RetrofitError err) {
                Log.d(TAG, "Request failed: " + job.getRequest(), err.getCause());
                return new ErrorEvent<Request>(job.getTicket(), job.getRequest(), err.getMessage());
            }
        }

        public void cancelAllJobs(boolean includeCurrent) {
            synchronized (this) {
                removeCallbacksAndMessages(null);
                if (includeCurrent && mCurrentJob != null) {
                    mCurrentJob.cancel();
                }
            }
        }
    }

    private static final String TAG = CloudikeClient.class.getSimpleName();

    private static final String ENDPOINT = "https://be-saas.cloudike.com/api/1/";

    private static CloudikeClient sInstance;

    private CloudikeApi mApiEngine;

    private WorkerHandler mHandler;

    private EventBus mEventBus;

    private class Authorizer implements RequestInterceptor {
        @Override
        public void intercept(RequestFacade request) {
            if (Session.getToken() != null) {
                request.addHeader("Mountbit-Auth", Session.getToken());
            }
        }
    }

    public static synchronized CloudikeClient getInstance() {
        if (sInstance == null) {
            sInstance = new CloudikeClient();
        }
        return sInstance;
    }

    private Authorizer mAutorizer = new Authorizer();

    private CloudikeClient() {
        mEventBus = new EventBus(ThreadEnforcer.ANY);
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(LogLevel.FULL)
                .setRequestInterceptor(mAutorizer)
                .build();
        mApiEngine = restAdapter.create(CloudikeApi.class);
        HandlerThread thread = new HandlerThread(TAG);
        thread.start();
        mHandler = new WorkerHandler(thread.getLooper());
    }


    public synchronized void register(Object object) {
        mEventBus.register(object);
    }

    public synchronized void unregister(Object object) {
        mEventBus.unregister(object);
    }

    public void cancelPendingRequests(boolean cancelCurrent) {
        mHandler.cancelAllJobs(cancelCurrent);
    }

    public Job executeRequest(Request request, Bundle options) throws SessionExpiredException {
        if (Session.isExpired()) {
            throw new SessionExpiredException("Session Expired, you have to re-login!");
        }
        Job job = new Job(request, options);
        mHandler.runJob(job);
        return job;
    }


    public Job executeRequest(Request request) throws SessionExpiredException {
        return executeRequest(request, new ParamsBuilder().build());
    }

    private BaseEvent executeJob(Job job) {
        Request request = job.getRequest();
        Bundle args = job.getOptions();
        RequestTicket ticket = job.getTicket();
        HashMap<String, String> pathParams = (HashMap<String, String>) args.getSerializable(ParamsBuilder.PATH_PARAMS);
        HashMap<String, String> postParams = (HashMap<String, String>) args.getSerializable(ParamsBuilder.POST_PARAMS);
        HashMap<String, String> getParams = (HashMap<String, String>) args.getSerializable(ParamsBuilder.GET_PARAMS);
        switch (request) {
            case LOGIN:
                LoginData login = mApiEngine.login(postParams);
                return new LoginEvent<LoginData>(ticket, login);
            case GET_METADATA:
                String path = pathParams.get(FileMetadataParamsBuilder.PATH);
                FileMetadata meta = null;
                if (path != null) {
                    meta = mApiEngine.getMetadata(path, getParams);
                } else {
                    meta = mApiEngine.getMetadata(getParams);
                }
                return new FileMetadataEvent<FileMetadata>(ticket, meta);
        }
        return new ErrorEvent<Request>(ticket, request, "Unsupported api call!");
    }

}
