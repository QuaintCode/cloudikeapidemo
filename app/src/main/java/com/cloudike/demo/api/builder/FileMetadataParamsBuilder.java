package com.cloudike.demo.api.builder;

public class FileMetadataParamsBuilder extends ParamsBuilder {

    public static final String PATH = "path";
    public static final String HASH = "hash";
    public static final String LISTING = "listing";
    public static final String DIRS_ONLY = "dirs_only";
    public static final String DELETED = "deleted";
    public static final String VERSION = "version";
    public static final String EXTRA = "extra";
    public static final String OFFSET = "offset";
    public static final String LIMIT = "limit";
    public static final String ORDER_BY = "order_by";

    public enum SortOrder {

        BY_NAME("name"),
        BY_NAME_INVERSE("-name"),
        BY_MOD_TIME("modified_time"),
        BY_MOD_TIME_INVERSE("-modified_time");

        private final String mOrder;

        private SortOrder(final String text) {
            mOrder = text;
        }

        @Override
        public String toString() {
            return mOrder;
        }
    }

    public FileMetadataParamsBuilder addPath(String path) {
        addPathParam(PATH, path);
        return this;

    }

    public FileMetadataParamsBuilder addHash(String hash) {
        addGetParam(HASH, hash);
        return this;

    }

    public FileMetadataParamsBuilder addListing(boolean listing) {
        addGetParam(LISTING, String.valueOf(listing));
        return this;

    }

    public FileMetadataParamsBuilder addDirsOnly(boolean dirsOnly) {
        addGetParam(DIRS_ONLY, String.valueOf(dirsOnly));
        return this;

    }

    public FileMetadataParamsBuilder addDeleted(boolean deleted) {
        addGetParam(DELETED, String.valueOf(deleted));
        return this;

    }

    public FileMetadataParamsBuilder addVersion(int version) {
        addGetParam(VERSION, String.valueOf(version));
        return this;

    }

    public FileMetadataParamsBuilder addExtra(boolean extra) {
        addGetParam(EXTRA, String.valueOf(extra));
        return this;

    }

    public FileMetadataParamsBuilder addOffset(int offset) {
        addGetParam(OFFSET, String.valueOf(offset));
        return this;

    }

    public FileMetadataParamsBuilder addLimit(int limit) {
        addGetParam(LIMIT, String.valueOf(limit));
        return this;

    }

    public FileMetadataParamsBuilder addSortOrder(SortOrder order) {
        addGetParam(ORDER_BY, order.toString());
        return this;

    }
}
