package com.cloudike.demo.api.events;

import com.cloudike.demo.api.RequestTicket;

public class ErrorEvent<RequestType> extends BaseEvent {

    private RequestType mType;

    private String mErrMessage;

    public ErrorEvent(RequestTicket ticket, RequestType type, String errMsg) {
        super(ticket);
        mType = type;
        mErrMessage = errMsg;
    }

    @Override
    public RequestType getData() {
        return mType;
    }

    public String getErrMessage() {
        return mErrMessage;
    }
}
