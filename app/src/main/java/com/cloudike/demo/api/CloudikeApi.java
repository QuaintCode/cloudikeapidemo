package com.cloudike.demo.api;

import com.cloudike.demo.api.model.account.LoginData;
import com.cloudike.demo.api.model.files.FileMetadata;

import java.util.Map;

import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.QueryMap;

public interface CloudikeApi {

    @POST("/accounts/login/")
    @FormUrlEncoded
    LoginData login(@FieldMap Map<String, String> params);

    @GET("/metadata/{path}/")
    FileMetadata getMetadata(@Path("path") String path, @QueryMap Map<String, String> params);

    @GET("/metadata/")
    FileMetadata getMetadata(@QueryMap Map<String, String> params);
}
