package com.cloudike.demo.api.model.files;

import java.util.List;

public class FileMetadata {
    public static final String DOCUMENT_TEXT = "document_text";
    public static final String AUDIO = "audio";
    public static final String VIDEO = "video";
    private String owner_name;
    private Long created;
    private Boolean deleted;
    private String checksum;
    private Long author;
    private Long bytes;
    private String size;
    private Long modified;
    private String author_name;
    private String public_hash;
    private String hash;
    private String public_link;
    private Long version;
    private ExtraData extradata;
    private ClientData client_data;
    private Long owner;
    private String path;
    private Boolean folder;
    private Boolean shared;
    private String shared_hash;
    private String mime_type;
    private String icon;
    private boolean is_last_page;
    private List<FileMetadata> content;

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getPublicLink() {
        return public_link;
    }

    public void setPublicLink(String public_link) {
        this.public_link = public_link;
    }

    public String getSharedHash() {
        return shared_hash;
    }

    public void setSharedHash(String shared_hash) {
        this.shared_hash = shared_hash;
    }

    public Boolean getShared() {
        return shared;
    }

    public void setShared(Boolean shared) {
        this.shared = shared;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public boolean isIsLastPage() {
        return is_last_page;
    }

    public void setLastPage(boolean last) {
        this.is_last_page = last;
    }

    public String getOwnerName() {
        return owner_name;
    }

    public Long getCreated() {
        return created;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public String getChecksum() {
        return checksum;
    }

    public Long getAuthor() {
        return author;
    }

    public Long getBytes() {
        return bytes;
    }

    public Long getModified() {
        return modified;
    }

    public String getAuthorName() {
        return author_name;
    }

    public String getPublicHash() {
        return public_hash;
    }

    public Long getVersion() {
        return version;
    }

    public ExtraData getExtradata() {
        return extradata;
    }

    public ClientData getClientData() {
        return client_data;
    }

    public Long getOwner() {
        return owner;
    }

    public String getPath() {
        return path;
    }

    public Boolean isFolder() {
        return folder;
    }

    public String getMimeType() {
        return mime_type;
    }

    public String getIcon() {
        return icon;
    }

    public void setOwnerName(String ownerName) {
        this.owner_name = ownerName;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public void setAuthor(Long author) {
        this.author = author;
    }

    public void setBytes(Long bytes) {
        this.bytes = bytes;
    }

    public void setModified(Long modified) {
        this.modified = modified;
    }

    public void setAuthorName(String authorName) {
        this.author_name = authorName;
    }

    public void setPublicHash(String publicHash) {
        this.public_hash = publicHash;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public void setExtradata(ExtraData extradata) {
        this.extradata = extradata;
    }

    public void setClientData(ClientData clientData) {
        this.client_data = clientData;
    }

    public void setOwner(Long owner) {
        this.owner = owner;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setFolder(Boolean folder) {
        this.folder = folder;
    }

    public void setMimeType(String mimeType) {
        this.mime_type = mimeType;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<FileMetadata> getContent() {
        return content;
    }

    public void setContent(List<FileMetadata> content) {
        this.content = content;
    }

    public boolean isTextFile() {
        return DOCUMENT_TEXT.equals(getIcon());
    }

    public boolean isAudioFile() {
        return AUDIO.equals(getIcon());
    }

    public boolean isVideo() {
        return VIDEO.equals(getIcon());
    }
}
