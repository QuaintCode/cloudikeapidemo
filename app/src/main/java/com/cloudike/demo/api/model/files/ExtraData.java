package com.cloudike.demo.api.model.files;

import java.util.HashMap;
import java.util.Map;

public class ExtraData {
    private Thumbnails thumbnails;
    private Videos videos;
    private MetaInfo metainfo;

    public Thumbnails getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(Thumbnails thumbnails) {
        this.thumbnails = thumbnails;
    }

    public Videos getVideos() {
        return videos;
    }

    public void setVideos(Videos videos) {
        this.videos = videos;
    }

    public MetaInfo getMetainfo() {
        return metainfo;
    }

    public void setMetainfo(MetaInfo metainfo) {
        this.metainfo = metainfo;
    }

    public class Preview {

        private String link;

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }
    }

    public class Thumbnails {
        private String status;
        private Preview small;
        private Preview preview;
        private Preview middle;
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Preview getSmall() {
            return small;
        }

        public void setSmall(Preview small) {
            this.small = small;
        }

        public Preview getPreview() {
            return preview;
        }

        public void setPreview(Preview preview) {
            this.preview = preview;
        }

        public Preview getMiddle() {
            return middle;
        }

        public void setMiddle(Preview middle) {
            this.middle = middle;
        }

    }

    public class Videos {
        private String status;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

    public class MetaInfo {
        private String status;
        private Long video_bit_rate;
        private Long height;
        private String video_codec_name;
        private Long width;
        private Double duration;
        private String audio_codec_name;
        private Boolean video_stream;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Long getVideoBitRate() {
            return video_bit_rate;
        }

        public void setVideoBitRate(Long videoBitRate) {
            this.video_bit_rate = videoBitRate;
        }

        public Long getHeight() {
            return height;
        }

        public void setHeight(Long height) {
            this.height = height;
        }

        public String getVideoCodecName() {
            return video_codec_name;
        }

        public void setVideoCodecName(String videoCodecName) {
            this.video_codec_name = videoCodecName;
        }

        public Long getWidth() {
            return width;
        }

        public void setWidth(Long width) {
            this.width = width;
        }

        public Double getDuration() {
            return duration;
        }

        public void setDuration(Double duration) {
            this.duration = duration;
        }

        public String getAudioCodecName() {
            return audio_codec_name;
        }

        public void setAudioCodecName(String audioCodecName) {
            this.audio_codec_name = audioCodecName;
        }

        public Boolean getVideoStream() {
            return video_stream;
        }

        public void setVideoStream(Boolean videoStream) {
            this.video_stream = videoStream;
        }

    }
}
