package com.cloudike.demo.api.events;

import com.cloudike.demo.api.RequestTicket;

public class LoginEvent<LoginData> extends BaseEvent {

    private LoginData mLoginData;

    public LoginEvent(RequestTicket ticket, LoginData data) {
        super(ticket);
        mLoginData = data;
    }

    @Override
    public LoginData getData() {
        return mLoginData;
    }
}
