package com.cloudike.demo.api.builder;

public class LoginParamsBuilder extends ParamsBuilder {

    public static final String EMAIL = "email";

    public static final String PASSWORD = "password";

    public LoginParamsBuilder addEmail(String email) {
        addPostParam(EMAIL, email);
        return this;
    }

    public LoginParamsBuilder addPassword(String pass) {
        addPostParam(PASSWORD, pass);
        return this;

    }
}
