package com.cloudike.demo.api.exception;

public class SessionExpiredException extends Exception {

    public SessionExpiredException() {
    }

    public SessionExpiredException(String message) {
        super(message);
    }
}
