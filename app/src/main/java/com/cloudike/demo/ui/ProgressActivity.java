package com.cloudike.demo.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;

import com.cloudike.demo.R;

public class ProgressActivity extends Activity {
    // do not show progress dialog if request takes less than 500 msecs
    public static final int DELAY_MILLIS = 500;
    private ProgressDialog mProgress;
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
        mProgress = new ProgressDialog(this);
        mProgress.setMessage(getString(R.string.progress_dlg_text));
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);
    }

    protected void showProgress() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mProgress.show();
            }
        }, DELAY_MILLIS);
    }

    protected void hideProgress() {
        mHandler.removeCallbacksAndMessages(null);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mProgress.hide();
            }
        });
    }
}
