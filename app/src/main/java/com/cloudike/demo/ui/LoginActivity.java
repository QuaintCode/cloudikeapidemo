package com.cloudike.demo.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cloudike.demo.R;
import com.cloudike.demo.api.CloudikeClient;
import com.cloudike.demo.api.CloudikeClient.Request;
import com.cloudike.demo.api.Session;
import com.cloudike.demo.api.builder.LoginParamsBuilder;
import com.cloudike.demo.api.events.ErrorEvent;
import com.cloudike.demo.api.events.LoginEvent;
import com.cloudike.demo.api.exception.SessionExpiredException;
import com.cloudike.demo.api.model.account.LoginData;
import com.squareup.otto.Subscribe;

public class LoginActivity extends ProgressActivity {

    private EditText mEmailView;
    private EditText mPasswordView;
    private CloudikeClient mCloudike;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mCloudike = CloudikeClient.getInstance();
        mCloudike.register(this);


        mEmailView = (EditText) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);


        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
    }

    @Subscribe
    public void handleError(ErrorEvent<Request> error) {
        hideProgress();
        Toast.makeText(this, error.getData() + " failed", Toast.LENGTH_LONG).show();
    }

    @Subscribe
    public void handleLogin(LoginEvent<LoginData> login) {
        hideProgress();
        LoginData data = login.getData();
        Session.setToken(data.getToken(), data.getExpires());
        startActivity(new Intent(this, BrowserActivity.class));
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCloudike.unregister(this);
    }

    public void attemptLogin() {
        showProgress();
        LoginParamsBuilder builder = new LoginParamsBuilder()
                .addEmail(mEmailView.getText().toString())
                .addPassword(mPasswordView.getText().toString());

        try {
            mCloudike.executeRequest(Request.LOGIN, builder.build());
        } catch (SessionExpiredException e) {
            //will never happen during login
        }
    }

}



