package com.cloudike.demo.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudike.demo.R;
import com.cloudike.demo.api.CloudikeClient;
import com.cloudike.demo.api.builder.FileMetadataParamsBuilder;
import com.cloudike.demo.api.builder.ParamsBuilder;
import com.cloudike.demo.api.events.ErrorEvent;
import com.cloudike.demo.api.events.FileMetadataEvent;
import com.cloudike.demo.api.exception.SessionExpiredException;
import com.cloudike.demo.api.model.files.ExtraData;
import com.cloudike.demo.api.model.files.FileMetadata;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;
import java.util.Stack;

public class BrowserActivity extends ProgressActivity implements AdapterView.OnItemClickListener {

    private static final String TAG = BrowserActivity.class.getSimpleName();

    private FilesAdapter mAdapter;

    private ListView mList;

    private Stack<String> mStack = new Stack<String>();

    private CloudikeClient mClient;

    private String mCurrentPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);
        mList = (ListView) findViewById(R.id.files);
        mAdapter = new FilesAdapter(this, null);
        mList.setAdapter(mAdapter);
        mList.setOnItemClickListener(this);
        mClient = CloudikeClient.getInstance();
        mClient.register(this);
        ParamsBuilder emptyBuilder = new ParamsBuilder();
        execute(CloudikeClient.Request.GET_METADATA, emptyBuilder.build());
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        FileMetadata m = mAdapter.getItem(position);
        if (m.isFolder()) {
            mStack.push(mCurrentPath);
            FileMetadataParamsBuilder builder = new FileMetadataParamsBuilder();
            builder.addPath(m.getPath());
            execute(CloudikeClient.Request.GET_METADATA, builder.build());
        }
    }

    @Subscribe
    public void handleError(ErrorEvent<CloudikeClient.Request> error) {
        hideProgress();
        Toast.makeText(this, error.getData() + " failed", Toast.LENGTH_LONG).show();
    }

    @Subscribe
    public void handleFileMetadata(FileMetadataEvent<FileMetadata> meta) {
        hideProgress();
        mCurrentPath = meta.getData().getPath();
        mAdapter.update(meta.getData().getContent());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mClient.unregister(this);
    }

    private class FilesAdapter extends BaseAdapter {

        private List<FileMetadata> mData;
        private Context mContext;

        public FilesAdapter(Context c, List<FileMetadata> list) {
            mData = list;
            mContext = c;
        }

        @Override
        public int getCount() {
            return (mData != null) ? mData.size() : 0;
        }

        @Override
        public FileMetadata getItem(int position) {
            return mData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = View.inflate(mContext, R.layout.browser_item, null);
            }
            FileMetadata item = getItem(position);
            ImageView thumb = (ImageView) view.findViewById(R.id.thumb);
            Picasso.with(mContext).cancelRequest(thumb);

            if (item.isFolder()) {
                thumb.setImageResource(R.drawable.folder);
            } else {
                setFileThumb(item, thumb);
            }
            TextView fileName = (TextView) view.findViewById(R.id.text);
            fileName.setText(new File(item.getPath()).getName());
            return view;
        }

        private void setFileThumb(FileMetadata meta, ImageView thumb) {
            if (meta.isVideo()) {
                thumb.setImageResource(R.drawable.video);
            } else if (meta.isAudioFile()) {
                thumb.setImageResource(R.drawable.audio);
            } else if (meta.isTextFile()) {
                thumb.setImageResource(R.drawable.text);
            } else {
                thumb.setImageResource(R.drawable.file);
            }
            if (meta.getExtradata() != null && meta.getExtradata().getThumbnails() != null) {
                ExtraData.Preview preview = meta.getExtradata().getThumbnails().getSmall();
                if (preview != null) {
                    String url = preview.getLink();
                    Picasso.with(mContext).load(url).into(thumb);
                }
            }
        }

        public void update(List<FileMetadata> list) {
            mData = list;
            notifyDataSetChanged();
        }
    }

    @Override
    public void onBackPressed() {
        if (!mStack.isEmpty()) {
            String path = mStack.pop();
            FileMetadataParamsBuilder builder = new FileMetadataParamsBuilder();
            builder.addPath(path);
            execute(CloudikeClient.Request.GET_METADATA, builder.build());
        } else {
            super.onBackPressed();
        }
    }


    private CloudikeClient.Job execute(CloudikeClient.Request req, Bundle opt) {
        showProgress();
        try {
            return mClient.executeRequest(req, opt);
        } catch (SessionExpiredException e) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
        return null;
    }

}
